# GPGPU-Sim for Penny
This is a modified version of GPGPU-Sim for Penny. Additional features added:

* Reads Penny-generated resource allocation information.
* Can explicit external PTX file to be executed as the device code by setting PTX\_SIM\_USE_PTX\_FILE, PTX\_SIM\_KERNELFILE, PTX\_SIM\_KERNEL\_INFO.


## Configuration
Set the variables in source_me.env correctly for each system.  Source this before building the simulator or executing a simulation. Most important variables are:

* GCC\_DIR: Where GCC 4.6 or 4.7 resides.
* CUDA\_INSTALL\_PATH: Where CUDA Toolkit  5.5 is installed.
* NVIDIA\_COMPUTE\_SDK\_LOCATION: Where the sample sources for CUDA 5.5 is installed.

## Build
1. Source the configured environment variables in bash.

        source source_me.env

2. Build the simulator using make.

        make

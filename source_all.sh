scrdir=$( cd "$(dirname ${BASH_SOURCE[0]} )" && pwd)
source $HOME/gcc-4.6.env
export LD_PRELOAD=/usr/lib64/libstdc++.so.6
source $scrdir/cuda_env.sh
source $scrdir/setup_environment debug
#source $scrdir/tools.sh

export DATA_DIR=${HOME}/gpu_exp
